FROM python:3.7.4-buster

LABEL maintainer="Laury de Donato <l2do@my.email>, Clément Joly <cj@my.email>"

RUN pip install pip --upgrade; \
    pip install setuptools --upgrade

# Copy application files to /var/local/app
COPY app /var/local/app/
# RUN chmod +x /var/local/app/*.sh

# Install dependencies into the application virtual environement
RUN pip install -r /var/local/app/requirements.txt

WORKDIR /var/local/app/
CMD python main.py
